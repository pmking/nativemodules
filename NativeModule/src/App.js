import React, { Component } from 'react';
import { Button, Text, View } from 'react-native';
import ActivityOpen from '../modules';


export default class App extends Component {
    constructor(props){
        super(props)
        this.onOpenA.bind(this)
        this.onOpenB.bind(this)
    }
    onOpenA(){
        ActivityOpen.openActivity("A","A Component")
    }

    onOpenB(){
        ActivityOpen.openActivity("B", "B Component")
    }

    render() {
        return (
            <View style={{justifyContent:'space-around',alignItems:'center',flex:1,flexDirection:'row'}}>
                <Button
                    
                    onPress={this.onOpenA}
                    title="OPEN A"
                    color="#841584"
                />           
                
                <Button
                    onPress={this.onOpenB}
                    title="OPEN B"
                    color="#841584"
                />  
                
            </View>
        )
    }
}