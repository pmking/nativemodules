package com.nativemodule.custom_module;

import android.content.Intent;
import android.text.TextUtils;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.nativemodule.NewActivity;

/**
 * Created by prashantmishra on 16/07/18.
 */

public class ActivityModule extends ReactContextBaseJavaModule {
    public ActivityModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "ActivityOpen";
    }

    @ReactMethod
    public void openActivity(String type , String title){
        if(!TextUtils.isEmpty(type)){
            ReactApplicationContext context = getReactApplicationContext();

            final Intent intent = newActivityIntent(context);

            if(type.equals("A")){

                intent.putExtra("data",type);
                intent.putExtra("title",title);
                context.startActivity(intent);

            }else {

                intent.putExtra("data",type);
                intent.putExtra("title",title);
                context.startActivity(intent);
            }
        }
    }

    private Intent newActivityIntent(ReactApplicationContext context){
        final Intent newDocumentIntent = new Intent(context, NewActivity.class);
        newDocumentIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        // Always launch in a new task.
        newDocumentIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        return newDocumentIntent;
    }
}
