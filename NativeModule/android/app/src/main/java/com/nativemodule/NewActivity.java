package com.nativemodule;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by prashantmishra on 16/07/18.
 */

public class NewActivity extends AppCompatActivity {
    TextView tv = null;
    String title ="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        tv = findViewById(R.id.tv_data);
        String extra = getIntent().getStringExtra("data");
        title = getIntent().getStringExtra("title");
        tv.setText("we are inside native activity "+extra);

        setTaskDescription(newTaskDescription(extra));

    }

    private ActivityManager.TaskDescription newTaskDescription(String extra) {
        if(extra.equals("A")){
            Bitmap newIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_a);
            String newDocumentRecentsLabel = title;
            ActivityManager.TaskDescription newTaskDescription = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                newTaskDescription = new ActivityManager.TaskDescription(
                        newDocumentRecentsLabel, newIcon);
            }
            return newTaskDescription;
        }else {
            Bitmap newIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_b);
            String newDocumentRecentsLabel =title ;
            ActivityManager.TaskDescription newTaskDescription = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                newTaskDescription = new ActivityManager.TaskDescription(
                        newDocumentRecentsLabel, newIcon);
            }
            return newTaskDescription;
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }
}
